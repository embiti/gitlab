# frozen_string_literal: true

module QA
    RSpec.describe 'Manage', :requires_admin do
       describe 'Import project from Bitbucket Cloud' do
            it 'imports a Bitbucket repo' do
                Flow::Login.sign_in
                Page::Main::Menu.perform(&:go_to_projects)

                click_on(class: 'gl-button')

                Page::Project::New.perform(&:click_import_project)

                click_link('Bitbucket Cloud')

                expect(page).to have_content("Import projects from Bitbucket")
            end
       end
    end
end


